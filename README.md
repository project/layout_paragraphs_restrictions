# Layout Paragraphs Restrictions

This provides a means of restricting Paragraph types that can be placed within Layout Paragraph instances by matching context variables. Restrictions may be placed on entire layouts or on regions within layouts. For example, you may wish to restrict the placement of a 'Hero' Paragraph type only to be allowed within a one-column layout or a full-page-width region within a two-column layout.

## Configuration

Visit `/admin/config/content/layout-paragraphs/restrictions` and enter the restriction definitions as yml.
Restriction definitions are defined as named combinations of contexts and a list of components that should be allowed or prohibited in that context.

See `example.layout_paragraphs_restrictions.yml` for the correct syntax.

## Contexts

Contexts are defined as a list of context variables that must be matched in order for the restriction to apply.
Valid values include the following:
- `parent_uuid`: The UUID of the parent component.
- `parent_type`: The bundle of the parent component.
- `sibling_uuid`: The UUID of the sibling component.
- `sibling_type`: The bundle of the sibling component.
- `region`: The region name (_root if no region is set).
- `layout`: The layout plugin ID.
- `placement`: The placement of the component (before or after).

## Components

Components are defined as a list of component machine names. Components are the Paragraph types that are allowed or
prohibited in the context.

## Examples
### Restricting an entire layout component
```yaml
my_restriction_rule:                  # The name of the restriction rule.
  context:
    parent_type: section              # The parent Layout Paragraph type. In this case, a "section".
    components:                       # The list of components that are allowed in the context.
      - rich_text
      - image
      - call_to_action
```
In this example, the `parent_type` context is set to a Layout Paragraph called `section`. The components `rich_text`, `image`,
and `call_to_action` are allowed to be placed within a `section` Layout Paragraph.

### Restricting specific regions within layouts
```yaml
full_width_onecol:
  context:
    layout: onecol                    # The machine name of the layout.
    region: content                   # The machine name of the region within the layout.
  components:
    - hero
full_width_twocol:
  context:
    layout: twocol
    region: top
  components:
    - hero
```
In this example, a `hero` component is allowed to be placed within the content region of a one-column layout and the top
region of a two-column layout.

### Excluding components instead of including

```yaml
no_accordion:
  context:
    parent_type: section
  exclude_components:
    - accordion
```
In this example, the `accordion` component is not allowed to be placed within a `section` Layout Paragraph, but all other
components are allowed.

## Dependencies

- [Layout Paragraphs](https://www.drupal.org/project/layout_paragraphs)
